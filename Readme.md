# Coding Standards

## Generic
- Comments must always make sense in English and contriute something meaningful to reading the code.

## Go
- Static checking
  - Must compile
  - Must pass gofmt
  - Must pass govet
  - Must pass golint

+ Testing
  - Unit
    - 100% coverage with go test / go tool cover modulo things that would require mocking
  - Integration / API
    - Use go test to hit API (gRPC/http/…)
    - Should cover 100% of API
    - Use the lightest weight possible environment

## CI
  + All code shall be built in a clean container
  + To the extent possible, unit tests shall be run in clean containers
  + Tests must be concurrency safe
  + Artifacts
    + Live forever
    + All built binaries
    + All software must have one of the following distribution mechanisms
      - Container
      - Debian package

## Git
- Tagline less than 50 chars
- Log comment 72 char line limits (pre-hooks)
- All merges to master accompanied by semantic version tag (CI enforced?)
- MRs to master should be up to date with master
- Commit as yourself
- Log messages in master branches should be descriptive
- Live code should always reflect a stable and documented tag
- No MR to master without green CI
- Commits to master must be signed off by committer

## Python
- Shall be Python 3
- Shall conform to the Merge pylint spec
- Shall contain setup.py for packaging
- Shall specify version constraints on dependencies
- Testing
  + Unit
  + 100% coverage with go test / go tool cover modulo things that would require mocking

## Javascript / Web
- ESLINT AirBnB
- 508 Accessibility Compatibility
- Lighthouse
  + Performance
  + Accessibility
  + Best Practices
  + Mobile compatibility
- Testing
  + Unit
    - 100% coverage
  + Selenium
  + Appium

## Language choice guidelines
- Services and programs should be written in a statically typed language.
  + Current preference is for Go.
- Current scripting language preference is Python 3.

## Containers
Containers used in produciton shall always be [semantically versioned](https://semver.org/). **The tags 
`latest` and `master` are explicitly forbidden for production**. Only tags of the 
form 
```
v<MAJOR>.<MINOR>.<PATCH>
```
are allowed.
