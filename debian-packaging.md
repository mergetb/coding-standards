# Debian Packaging

In general, MergeTB technologies should be packaged to work on Debian. This
document describes how to create and deploy Debian packages for MergeTB
technologies.

## Creating debian packages

First create a directory called `debian` in the top level of your source
repository. Then create the following files.

### Required package files

- [control](https://gitlab.com/mergetb/tech/canopy/blob/master/debian/control)
  provides basic information about your package.
- [compat](https://gitlab.com/mergetb/tech/canopy/blob/master/debian/compat)
  specifies the Debian package compatability level. Use `10`.
- [changelog](https://gitlab.com/mergetb/tech/canopy/blob/master/debian/changelog)
  informs users of changes across package versions. The Debian packing tools are
  very picky about the format of this file, pay attention to the details.
- [copyright](https://gitlab.com/mergetb/tech/canopy/blob/master/debian/copyright)
  describes the copyright status of the source files in your repository. If you
  don't have a license already setup, Apache 2.0 is a good choice as that is
  what most other Merge technologies use. Close source licenses are forbidden for 
  Merge technologies.
- [rules](https://gitlab.com/mergetb/tech/canopy/blob/master/debian/rules)
  tells the Debian packaging tools how to build your package. The the rules 
  examples below for further insight.

### Optional package files

- A [systemd service file](https://gitlab.com/mergetb/tech/canopy/blob/master/debian/canopy.service) 
  should be provided with any packages that are intended to run as a daemon.
- For projects that have multiple packages, [install](https://gitlab.com/mergetb/tech/canopy/blob/master/debian/canopy-server.install)
  files must be provided for each package to differentiate what files go where.
  Don't be sloppy and just place all files in all packages.

### Build automation

In the top level of your repository, create a file called
[build-deb.sh](https://gitlab.com/mergetb/tech/canopy/blob/debian-packaging/build-deb.sh)
that creates the packages from your repository. For repositories that provide
packages for several architectures, all architectural variants should be
produced by this script.

Note that this will place the various package build artifacts (which include a
bit more than just the packages themselves) in the directory **above** the
source directory. This is probably not what is desired so you should move them
to a suitable location, especially for automatic artifact pulishing purposes. An
example of doing this is in the `build-deb.sh` file linked above.

### Control files

The most ciritical thing about control files is they define what the build and
runtime dependencies of your package are. Be sure and test that you can build
your package in a clean environment by only installing the `Build-Depends`
packages. There is a tool from the Debian packaging utilities that will
automatically install all build dependencies listed in a control file

```shell
mk-build-deps --install debian/control
```


### Rules files

The rules file tells the Debian packaging tools how to build your software. The
Debian packaging tools provide support for wrapping most build systems. You can
also enable detection of installable components such as systemd services with
feature gates. Here are some useful examples of using the `dh` utility to build
your package.

- **systeemd**
```make
dh $@ --with-systemd
```

- **python setup utlis integration**
```make
dh $@ --with python3 --buildsystem=pybuild
```

- **vanilla make or autotools**
```make
dh $@
```

### Makefiles

When using vanilla makefiles for your project, you must ensure that the
`install` target is compatibile with the Debian `dh` packaging utility. In most
cases this amounts to ensuring that the `DESTDIR` and `prefix` variables are
honored. See [this](https://gitlab.com/mergetb/tech/canopy/blob/debian-packaging/Makefile#L114) 
example.

### Building debian packages

Debain packages should be built in a clean Debian container. From the MergeTB
GitLab CI environment using the following on your build job will ensure this.

```yaml
tags:
 - merge
 - docker
 - buster
```

Publish the resulting 

- `.deb`
- `.buildinfo`
- `.build`
- `.changes`

as GitLab CI artifacts. These artifacts serve two fold, for deployment to the
Merge Debian package server (next section), and for testing. It's critically
important that these packages actually get used in follow on integration tests
that are launched for CI so that we know what are users are getting is what has
been tested at the packaging level.

## Deploying debian packages

MergeTB has a Debian package server located at https://pkg.mergetb.net. This
package server is built on [Aptly](https://www.aptly.info/). It exposes a REST
API for integration from CI jobs, that API is located at
https://pkg-api.mergetb.net. This API is protected by an API key and can only be
used from official MergeTB CI jobs.

The package server has to repositories

- **mergetb**: is the stable repo
- **mergetb-next**: is the testing repo

Individual MergeTB technologies should only ever push to mergetb-next. When a
major Release of the MergeTB technology stack as a whole is announced, the 
`mergetb-next` repository is snapshot and published to `mergetb`.

### Package signing

All packages that get deployed to the https://pkg.mergetb.net must be signed by
the MergeTB GPG key. Environment variables are available in the official MergeTB
GitLab CI environment to import and use these keys. These are protected
varaiables that are only accessible from protected branches. Importing these
variables from a GitLab CI script can be done as follows.

```yaml
- echo $MERGETB_GPG_KEY | base64 -di | gpg --import
- echo $MERGETB_GPG_PUBKEY | base64 -di | gpg --import
```

Signing the packages themselves can be done like so

```yaml
- apt-get update
- apt-get install -y devscripts dpkg-sig gnupg
- dpkg-sig -k $MERGETB_GPG_KEY_ID --sign builder build/*.deb
```

All of the `MERGETB_` environment variables are provided by the CI environment.

### Package deployment

Deploying packagtes to https://pkg.mergetb.net from the CI environment is a two
step process. First the deploy script must be fetched.

```yaml
- apt-get update
- apt-get install -y curl
- 'curl -H "PRIVATE-TOKEN: $REPO_READ_TOKEN" "$DEB_PUB_SCRIPT_URL" > add-deb.sh'
- 'chmod +x add-deb.sh'
```

The `REPO_READ_TOKEN` and `DEB_PUB_SCRIPT_URL` are provided by the CI
environment. Now that the `add-deb.sh` script has been fetched, it can be used
to deploy packages. Assuming your packages are in the directory `build`:

```yaml
- find build -name "*.deb" | xargs -n 1 ./add-deb.sh
```

All of the deployment machinery described above can be seen in an integated
example [here](https://gitlab.com/mergetb/tech/canopy/blob/debian-packaging/.gitlab-ci.yml)
